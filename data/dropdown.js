export const popovers = {
  gettingStarted:[
    {
      name: 'A. About Your Motorcar Insurance Policy',
      description: 'Welcome to Standard Insurance. As a motorcar policyholder, you have a copy of your insurance policy in electronic form or a printed copy.',
      underList: 'Some reminders to note in your motorcar policy:',
    },
    {
      name: '1. Your personal details.',
      description: 'Full name, complete address, email address and mobile number. Kindly make sure the details are correct to help us provide you with services necessary for your policy, if needed.',
    },
    {
      name: '2. Details of the insured unit.',
      description: 'Kindly make sure that the details reflected in the policy are correct to avoid any problems in case of a claim.',
    },
    {
      name: '3. QR Code in your policy',
      description: 'Welcome to Standard Insurance. As a motorcar policyholder, you have a copy of your insurance policy in electronic form or a printed copy',
    },
    {
      name: 'B. About Roadside Assistance Program',
      description: 'The Standard Insurance Roadside Assistance Program (RAP) is an exclusive towing service for motorcar insurance policyholders. If you are enrolled in the RAP, you simply need to call the 24-hour Customer Hotline (02) 8845-1111 for towing services. You may refer to www.standard-insurance.com for a complete menu of services covered by our RAP.',
    },
  ],
  aboutMotorPolicy:[
    {
      name: '1. Compulsory Third Party Liability (CTPL)',
      description: 'This is the basic insurance coverage required by the Land Transportation Office when you register your vehicle. Coverage is limited to liabilities arising from the death and/or bodily injury of a Third Party victim in an accident caused by the insured vehicle. The coverage also provides victims of vehicular accidents immediate source of relief without the necessity of proving fault or negligence of the insured driver. This coverage will answer for death, disablement and medical expenses of a person injured because of an accident involving your vehicle.',
    },
    {
      name: '2. Own Damage / Theft',
      description: 'This coverage protects your vehicle from Accident Collision and Theft, as well as protection from damages caused by Fire, Explosion, Lightning and Self-Ignition and Malicious Damage.',
    },
    {
      name: '3. Acts of Nature',
      description: 'This coverage will protect your car against damages directly caused by typhoon, floor, hurricane, volcanic eruption, earthquake of tother convulsions of nature.',
    },
    {
      name: '4. Excess Bodily Injury',
      description: 'This coverage answers for indemnities beyond the limits set forth under the CTPL coverage that protects the assured against liability of death or bodily injury to third party arising from an accident, cause by the insured vehicle. This serves also as an additional coverage on top of your basic CTPL, since this will answer for expenses in excess of CTPL’s coverage.',
    },
    {
      name: '5. Third Party Property Damage',
      description: 'This provides you with protection in case your vehicle damages other people’s properties. Usually, this means damage to someone else’s car, but it also includes damages to lamp post, telephone poles, fences, buildings or other structures your car hit.',
    },
    {
      name: '6. Unnamed Passenger Personal Accident',
      description: 'This provides you with protection in case your vehicle damages other people’s properties. Usually, this means damage to someone else’s car, but it also includes damages to lamp post, telephone poles, fences, buildings or other structures your car hit.',
      sub: 'Benefits provided are as follows: ',
    },  
  ],
}