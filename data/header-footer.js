export const navigation = {
  nav:[
    {name:'Contact Support', href:'#',},
    {name:'(+632) 8845-1111', href:'#',}
  ],
  socialLink:[
    {
      name:'facebook',
      href:'#', 
      icon:'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/icon-facebook-zdyobghzvc-rseddhxbii.png',
    },
    {
      name:'instagram',
      href:'#', 
      icon:'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/instagram-icon-2-akjugydteq.png',
    },
    {
      name:'linked in',
      href:'#', 
      icon:'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/icon-linkedin-arjwadgtem-wyziujfgbf.png',
    },
    {
      name:'youtube',
      href:'#', 
      icon:'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/icon-youtube-hzxtlqejbt-bvlryfghne.png',
    },
  ]
}