export const supportLinks = [
  {name: 'Motor', src: 'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/motor-skkwblkucx.png',},
  {name: 'House Insurance', src: 'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/home-insurance-aixjzaufvw.png',},
  {name: 'Motorcycle Insurance', src: 'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/motorcycle-insurance-yejyoswacw.png',},
  {name: 'Accident Insurance', src: 'https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/accident-insurance-oxszzoalbg.png',}
]