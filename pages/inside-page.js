import ContainerWrapper from "../component/container/ContainerWrapper";
import MainHead from "../component/MainHead";
import PageInsidePage from "../component/page/PageInsidePage";

const InsidePage = () => {
  return (
    <>
      <MainHead title="Inside Page" />
      <ContainerWrapper>
        <PageInsidePage />
      </ContainerWrapper>
    </>
  );
}

export default InsidePage;