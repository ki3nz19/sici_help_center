import ContainerWrapper from '../component/container/ContainerWrapper'
import MainHead from '../component/MainHead'
import PageLandingPage from '../component/page/PageLandingPage'

export default function Home() {
  return (
    <>
      <MainHead title="Help Center" />
      <ContainerWrapper>
        <PageLandingPage />
      </ContainerWrapper>
    </>
  )
}
