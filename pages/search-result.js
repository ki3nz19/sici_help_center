import ContainerWrapper from "../component/container/ContainerWrapper";
import MainHead from "../component/MainHead";
import PageSearchResult from "../component/page/PageSearhResult";

const SearchResult = () => {
  return (
    <>
      <MainHead title="Search Result" />
      <ContainerWrapper>
        <PageSearchResult />
      </ContainerWrapper>
    </>
  );
}

export default SearchResult;