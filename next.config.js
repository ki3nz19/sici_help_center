module.exports = {
  reactStrictMode: true,
  images: {
		domains: [
			'localhost',
			'glr-sbn.s3.ap-southeast-1.amazonaws.com',
			'glr-development.s3.ap-southeast-1.amazonaws.com',
		],
		imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
	}
}
