
import { Disclosure } from "@headlessui/react";
import { navigation } from "../../data/header-footer";


const ContainerHeader = () => {
  return (
    <>
      <Disclosure as="nav" className="bg-white shadow">
      {({ open }) => (
        <>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="flex justify-between">
              <div className="flex">
                <div className="flex-shrink-0 flex items-center py-4 sm:py-6">
                  <img
                    className="sm:h-12 h-8 w-auto"
                    src="https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/sici-logo-nxsyjvtxxt.png"
                    alt="Workflow"
                  />
                </div>
                
              </div>
              <div className="sm:ml-6 flex gap-x-4 sm:gap-x-8">
                {navigation.nav.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    className="navbar-menu-link inline-flex items-center text-xs sm:text-sm font-medium"
                  >
                    {item.name}
                  </a>
                ))}
              </div>
            </div>
          </div>
        </>
      )}
    </Disclosure>
    </>
  );
}

export default ContainerHeader;