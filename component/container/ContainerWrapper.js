import ContainerFooter from "./ContainerFooter";
import ContainerHeader from "./ContainerHeader";

const ContainerWrapper = ({children}) => {
  return (
    <>
      <div className="relative overflow-hidden">
        <ContainerHeader />
          <main className="relative">
            {children}
          </main>
        <ContainerFooter />
      </div>
    </>
  );
}

export default ContainerWrapper;