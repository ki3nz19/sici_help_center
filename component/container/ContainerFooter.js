import { navigation } from "../../data/header-footer";

const ContainerFooter =()=> {
  return (
    <>
      <footer className="bg-black" aria-labelledby="footer-heading">
        <div class="bg-cta">
          <div className="grid md:flex md:justify-center gap-6 sm:gap-8 px-6 max-w-7xl mx-auto py-6 sm:py-12">
            <div>
              <img src="https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/cta-01-pdbidtrlvv.png" />
            </div>
            <div>
              <img src="https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/cta-02-hbctviljkg.png" />
            </div>
          </div>
        </div>

        <div className="md:flex items-center md:justify-between max-w-7xl mx-auto py-6 text-center sm:text-left">
          <div className="flex gap-2 sm:gap-6 justify-center md:order-2">
            {navigation.socialLink.map((item) => (
              <a
                key={item.name}
                href={item.href}
                className="text-gray-400 hover:text-gray-300"
              >
                <span className="sr-only">{item.name}</span>
                <img src={item.icon} className="sm:h-6 sm:w-6 h-4 w-4" aria-hidden="true" />
              </a>
              
            ))}
          </div>
          <div className="sm:flex gap-4 md:gap-12 px-4">
            <p className="mt-4 font-semibold text-xs sm:text-sm text-gray-400 md:mt-0 md:order-1">
              &copy; 2021 STANDARD INSURANCE CO. INC.
            </p>
            <p className="mt-4 text-xs sm:text-sm font-semibold text-gray-400 md:mt-0 md:order-1">
              PRIVACY POLICY | terms and conditions
            </p>
          </div>
        </div>
      </footer>
    </>
  );
}

export default ContainerFooter;