import LandingPageHeader from "./landingPage/LandingPageHeader";

const PageLandingPage = () => {
  return (
    <>
      <LandingPageHeader />
    </>
  );
}

export default PageLandingPage;