import { searchList } from "../../../data/searchResult";

const SearchResultContent = () => {
  return (
    <>
      <div className="max-w-6xl mx-auto py-20 sm:py-36 px-12 grid gap-4 sm:gap-8">
        <h2 className="font-medium text-xl sm:text-3xl text-700">Your Search Results</h2>
        {searchList.map((list) => (
        <div>
          <h3 className="text-base sm:text-lg font-medium">{list.title}</h3>
          <p className="font-normal text-sm text-gray-400">{list.description}</p>
        </div>
        ))}
      </div>
    </>
  );
}

export default SearchResultContent;