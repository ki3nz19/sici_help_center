import HeaderSection from "../../content/HeaderSection";

const SearchResultHeader = () => {
  return (
    <>  
      <HeaderSection title="How can we help you?" src="https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/shutterstock-vshiuhdhra.png" />
    </>
  );
}

export default SearchResultHeader;