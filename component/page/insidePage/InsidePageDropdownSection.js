import AboutMotorPolicyPopoverList from "../../dropdown/popoverList/AboutMotorPolicyPopoverList";
import GettingStartedPopoverList from "../../dropdown/popoverList/GettingStartedPopoveList";
import MotorcarClaimServicingPopoverList from "../../dropdown/popoverList/MotorcarClaimServicingPopoverList";
import PublicPopover from "../../dropdown/PublicPopover";

const InsidePageDropdownSection = () => {
  return (
    <>
      <div className="max-w-4xl mx-auto px-4 pb-36 relative space-y-3">
        <div className="py-4 mb-9">
          <h2 className="text-center font-medium text-3xl sm:text-5xl">Motor</h2>
        </div>
        <PublicPopover title="Getting Started">
          <GettingStartedPopoverList />
        </PublicPopover>
        <PublicPopover title="About My Motor Policy">
          <AboutMotorPolicyPopoverList />
        </PublicPopover>
        <PublicPopover title="Motorcar Claims Servicing">
          <MotorcarClaimServicingPopoverList />
        </PublicPopover>
      </div>
    </>
  );
}

export default InsidePageDropdownSection;