import HeaderSection from "../../content/HeaderSection";
import { supportLinks } from "../../../data/insidePage";

const InsidePageHeader = () => {
  return (
    <>
      <HeaderSection title="How can we help you?" src="https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/sici-hero-jnjknhiaaw.png">
        <section
          className="-mt-32 max-w-7xl mx-auto relative z-10 pb-8 px-4 sm:px-6 lg:px-8"
          aria-labelledby="contact-heading"
        >
          <div className="grid grid-cols-1 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 lg:gap-y-2 lg:gap-x-8 justify-center">
            {supportLinks.map((link) => (
              <div key={link.name} className="flex justify-center ">
                <div className="relative justify-center">
                  <h3 id="contact-heading" className="sr-only">{link.name}</h3>
                  <img alt={link.name} src={link.src} className="" />
                </div>
              </div>
            ))}
          </div>
        </section>
      </HeaderSection>
    </>
  );
}

export default InsidePageHeader;