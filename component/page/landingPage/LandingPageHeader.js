import HeaderSection from "../../content/HeaderSection";

const LandingPageHeader = () => {
  return (
    <>
      <HeaderSection title="How can we help you?" src="https://glr-sbn.s3.ap-southeast-1.amazonaws.com/help_center/background-image-1-gbekaewlwb.png" />
    </>
  );
}

export default LandingPageHeader;