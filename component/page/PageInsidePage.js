import InsidePageDropdownSection from "./insidePage/InsidePageDropdownSection";
import InsidePageHeader from "./insidePage/InsidePageHeader";

const PageInsidePage = () => {
  return (
    <>
      <InsidePageHeader />
      <InsidePageDropdownSection />
    </>
  );
}

export default PageInsidePage;