import SearchResultContent from "./searchResult/SearchResultContent";
import SearchResultHeader from "./searchResult/SearchResultHeader";

const PageSearchResult = () => {
  return (
    <>
      <SearchResultHeader />
      <SearchResultContent />
    </>
  );
}

export default PageSearchResult;