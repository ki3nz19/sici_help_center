import InputSearchForm from "../input/InputSearchForm";

const HeaderSection = ({src, title, children}) => {
  return (
    <>
      <div className="bg-white">
        {/* Header */}
        <div className="relative pb-20 bg-gray-800">
          <div className="absolute inset-0">
            <img
              className="w-full h-full object-cover"
              src={src}
              alt="support img"
            />
          </div>
          <div className="relative max-w-7xl  mx-auto py-24 px-4 sm:py-32 sm:px-6 lg:px-8 space-y-4 text-center">
            <h1 className="text-4xl font-medium tracking-wider text-white md:text-5xl">{title}</h1>
            <div className="inline-block w-96 justify-center">
              <InputSearchForm 
                name="search"
                type="text"
                id="search"
                placeholder="Search Help Center..."
              />
            </div>
          </div>
        </div>

        {/* Overlapping cards */}
        {children}
      </div>
    </>
  );
}

export default HeaderSection;