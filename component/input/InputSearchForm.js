import {SearchIcon} from '@heroicons/react/outline';

const InputSearchForm = ({ name, label, placeholder, reference, type, defaultValue, disabled, required, onKeyPress, onChange } ) => {
  return (
    <>
      <div className="relative">
        { !!label &&
        <label htmlFor={ name } className="text-[#3A1700] text-base lg:text-lg" >{ label }</label>
        }
        <div className="mt-1">
          <input
            className="block w-full shadow-sm py-1.5 px-4 lg:text-lg text-base  text-gray-700 border focus:ring-[#E29F72] focus:border-[#E29F72] border-gray-200 rounded-full"
            id={ name }
            name={ name }
            placeholder={ placeholder }
            type={ type }
            ref={ reference }
            defaultValue={ defaultValue }
            disabled={ disabled }
            required={ required }
            onKeyPress={ onKeyPress }
            onChange={ onChange }
          />
        </div>
        <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
          <SearchIcon className="w-5 h-5 searc-icon" />
        </div>
      </div>
    </>
  );
}

export default InputSearchForm;