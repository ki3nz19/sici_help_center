import { popovers } from "../../../data/dropdown"

const GettingStartedPopoverList = () => {
  return (
    <>
      <ul role="list" className="mt-3">
        {popovers.gettingStarted.map((item) => (
          <li key={item.name} className="flow-root space-y-1">
            <p
              className="-ml-3 flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
            >
              <span className="ml-4">{item.name}</span>
            </p>
            <p className="font-normal text-sm ml-1 text-gray-600 mt-1">{item.description}</p>
            <p className="font-normal text-sm ml-1 text-gray-600 mt-1">{item.underList}</p>
          </li>
        ))}
      </ul>
    </>
  )
}

export default GettingStartedPopoverList;