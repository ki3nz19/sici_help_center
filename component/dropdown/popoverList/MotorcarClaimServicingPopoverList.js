import { popovers } from "../../../data/dropdown"

const MotorcarClaimServicingPopoverList = () => {
  return (
    <>
      <ul role="list" className="mt-3">
        <li className="flow-root space-y-1">
          <p
            className="-ml-3 flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-4 font-bold">What should I do if I have a claim?</span>
          </p>
        </li>
        <li className="text-sm ml-1 text-gray-600 mt-1">
          In order to service you well and to protect your rights being a policy holder, please take note of the following guidelines in filing a claim:
        </li>
        <li>
          <ul className="list-decimal ml-5 space-y-2">
            <li className="text-sm  text-gray-600 mt-1">
              In the event of an accident, loss or damage, you are strongly advised to take pictures to document the incident while it is still fresh.
              <ul className="space-y-2">
                <li>
                  a. For motor vehicular accidents
                  <ul className="ml-5 space-y-1">
                    <li>
                      i. Take photos of the site of the accident, the involved parties and/or property, showing the plate number, point of impact, the damages sustained and the final resting of position of the involved vehicles.
                    </li>
                    <li>
                      ii. If there is another vehicle involved, get the full particulars of the other vehicle. Most importantly, the contact number and address of the driver. Take a photo of the driver’s license and the Car Registration and Official Receipt if possible.
                    </li>
                    <li>
                      iii. If there is an injured person involved or accident resulted to death of any person, get full particulars of the victim and/or the legal heirs.
                    </li>
                  </ul>
                </li>
                <li>
                  b. For theft cases
                  <ul className="ml-5 space-y-1">
                    <li>
                      i. Picture of the place where it was discovered and a photo of the portion of the vehicle from where a car part was forcibly taken or stolen.
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li className="text-sm ml-1 text-gray-600 mt-1">
              Report or file your claim  to any of the following:
              <ul className="space-y-2">
                <li>
                  a. You may call our Customer Service Hotline at 845-1111.
                </li>
                <li>
                  b. Inform your agent, dealer or broker
                </li>
                <li>
                  c. Call or visit any Standard Insurance branch located in key cities nationwide
                </li>
                <li>
                  d. Call the Standard Insurance Trunk Line No. (02) 8988-6388 press option “2” for motor claims assistance
                  <ul className="ml-5 space-y-1">
                    <li>
                      i. When you call our Head Office, our Claims Customer Support Specialist will:
                      <ul className="ml-5 space-y-1 list-disc">
                        <li>
                        arrange the inspection of the insured unit and/or third party unit for assessment of the extent of damage;
                        </li>
                        <li>
                        give you the address and contact number of the nearest Standard Insurance Branch where you can file your claim documents; or
                        </li>
                        <li>
                        advise you of our accredited car dealers/repairers where you can bring your vehicle for repair.
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  e. Go to our website at www.standard-insurance.com and click report a claim.
                  <ul className="ml-5 space-y-1">
                    <li>
                      i. You will need photos of the documents to file a claim (please refer to the list of claim requirements on page ___) and 
                      photos of the damaged unit showing the vehicle’s full body, registration plate number/conduction sticker/VIN plate, odometer reading and damaged portion/s.
                    </li>
                    <li>
                      ii. You also need to indicate the origin, destination and purpose of travel during the accident when filing the loss/accident online notification.
                    </li>
                    <li>
                      iii. Upon filing online, you will get an SMS notification  on the status of your claim and you will be advised  if your unit will need an actual inspection.
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li className="text-sm ml-1 text-gray-600 mt-1">
              Immediately report the incident to the nearest police station and secure an original police report.
              <ul className="ml-5 space-y-1 list-disc">
                <li>
                  This is required if there are other parties involved e.g. injured third party persons, 
                  damaged property and/or third party vehicle/s, theft of a vehicle and its parts/accessories).
                </li>
                <li>
                  This is required if there are other parties involved e.g. injured third party persons, 
                  damaged property and/or third party vehicle/s, theft of a vehicle and its parts/accessories).
                </li>
                <li>
                For minor incidents (self-accident/minor collision, involving less than three (3) parts for replacement) 
                the driver of the insured unit may accomplish the SICI Motor Accident Report Form or execute an affidavit of loss/accident.
                </li>
              </ul>
            </li>
            <li className="text-sm ml-1 text-gray-600 mt-1">
              After notifying your claim, submit the claim requirements immediately.
              <ul className="ml-5 space-y-1 list-disc">
                <li>
                  All documents/requirements to be submitted must be clear and readable.
                </li>
                <li>
                  Car Registration renewal must be current. Your Insurer reserves the right to require additional/supplemental 
                  documents as may be deemed necessary to substantiate your claim.
                </li>
                <li>
                  Please indicate your preferred repairer or workshop from among our accredited dealers. You may contact our claims hotline for details.
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li className="flow-root mt-2">
          <p
            className="-ml-3 flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-4 font-bold">What are the requirements to file a claim? </span>
          </p>
        </li>
        <li className="text-sm ml-1 text-gray-600 mt-1">
          To enable us to quickly process your claim, please make sure to submit the complete 
          claim requirements and disclose all the information about the loss or incident when notifying us about a claim.
        </li>
        <li className="text-sm ml-1 text-gray-600 mt-1">
          The original or hard copies of the requirements especially the police report or driver’s affidavit may be submitted at the nearest Standard Insurance office most convenient to you.
        </li>
        <li className="text-sm ml-1 text-gray-600 mt-1">
          The following <span className="font-bold">BASIC</span> requirements must be submitted in case of a claim:
        </li>
        <li>
          <ul className="list-decimal ml-6 space-y-2">
            <li className="text-sm text-gray-600 mt-2">
              Duly accomplished notarized Standard Insurance Motor Accident Report Form or Duly Notarized Driver’s Affidavit (for minor collision, self-accident) OR 
              <ul className=" space-y-1">
                <li className="mt-1">
                  a. Original or certified true copy of Police Report (for accidents involving third parties or theft or severe vehicle damage).
                </li>
                <li className="mt-1">
                  b. Note: For fire incidents, Bureau of Fire Report is required.
                </li>
              </ul>
            </li>
            <li className="text-sm text-gray-600">
              Clear and readable copy of driver’s license and official receipt.
            </li>
            <li className="text-sm text-gray-600">
              Clear and readable copy of Certificate of Registration and latest official receipt.
            </li>
            <li className="text-sm text-gray-600">
              Clear and readable copy of the policy schedule and official receipt of premium payment.
            </li>
            <li className="text-sm text-gray-600">
              Photographs of the damaged vehicle taken at the time of the accident showing the final position and point of impact of the insured unit and other vehicles or properties involved (if any)
            </li>
            <li className="text-sm text-gray-600">
              For thru Picture Assessment:
              <ul className=" space-y-1">
                <li className="mt-1">
                  a. Stencils of motor and chassis number.
                </li>
                <li>
                  b. Photos of the full view of the vehicle showing its plate number, conduction sticker, VIN plate and odometer or kilometer reading.
                </li>
                <li>
                  c. Estimate (if any)
                </li>
              </ul>
            </li>
            <li className="text-sm text-gray-600">
              For incidents involving damage to other vehicles, property or injury/death additional documents will be required. (or we can also enumerate them in this material).
            </li>
            <li className="text-sm text-gray-600">
            Please be reminded that all documents/requirements to be submitted must be clear and readable. We reserve the right to require additional/supplemental documents as may be deemed necessary to substantiate your claim.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className=" flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Should you have any concerns please feel free to call our Customer Service Hotline at 8845-1111.</span>
          </p>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Important Reminders:</span>
          </p>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Full Disclosure</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            In adherence to the principle of good faith, you should fully disclose all information about the vehicular accident. Misrepresentation and/or concealment is a ground for claim rejection.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Immediate Claim Notification</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            To allow us to promptly service your claim while the incident is still fresh, immediately notify Standard Insurance (within 30 days from the date of the incident). For Acts of Nature claim, within 7 days.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Abandonment of Claim</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            Failure to submit the complete required documents within ninety (90) days from filing of claim, shall be construed as non-interest on your part to pursue the claim. After ninety (90) days, your claim shall be considered abandoned or closed.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">No reimbursement guarantee</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            In cases wherein, there is a third party involved, any negotiations and/or settlement made will not guarantee reimbursement.
            </li>
            <li className="text-sm text-gray-600">
            Avoid engaging in settlement and/or payments without the approval or consent of your Insurer. Please refer to Condition No. 4 of the General Exceptions of the Policy.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Inspection and claim approval prior to repair</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            Do not commence repairs on the insured unit and/or third party unit until inspected by a Technical Assistant of Standard Insurance.
            </li>
            <li className="text-sm text-gray-600">
            Take reasonable steps to protect the vehicle from further damage or loss of vehicle parts. A Letter of Authority to repair or a cash settlement proposal letter will be released by your Insurer after your claim has been assessed and approved.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Windshield claim</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            In case of windshield damage, and replacement of the same needs to be effected immediately, take photos of the insured unit showing its plate number and conduction sticker and the damaged portion of the windshield (in one photograph).
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Accredited Repairer / Authority to Pull-out / Storage Charges</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            Damaged insured unit and third party vehicle should be brought to any of Standard Insurance’s accredited dealerships or shops for repair.
            </li>
            <li className="text-sm text-gray-600">
            Repair of vehicle acquired from a Dealer tie-up of Standard Insurance shall be referred to the same Dealership.
            </li>
            <li className="text-sm text-gray-600">
            Should you have any concern on your vehicle repair, please let us know so we may coordinate with the Dealer or Repairer to address it.
            </li>
            <li className="text-sm text-gray-600">
            Feel free to contact us so you can be advised of the accredited dealerships and shops nearest you. Otherwise, 
            storage charges incurred at an unauthorized shop will be for your own personal account.
            </li>
            <li className="text-sm text-gray-600">
            Likewise, in case a vehicle is brought for repair at our accredited dealership or shop, and was eventually recommended as constructive total loss, 
            you are required to submit a letter authorizing Standard Insurance to pull-out or remove the car for transfer to our motor pool for safekeeping or 
            to your custody while the claim is in process. This is to avoid storage charges. 
            You shall bear storage charges if there is failure or delay in the submission of the said authorization letter or any other claim requirement.
            </li>
            <li className="text-sm text-gray-600">
            For vehicle with substantial amount of damage, 
            and was impounded and/or brought to a place other than which was authorized by Standard Insurance, 
            storage charges will be for your own personal account.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Duty of Insured to Safeguard the Insured Vehicle from Further Damage</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            In the event wherein the insured unit is immobilized and/or heavily damaged, or flooded/inundated do not try to start the engine or continue driving to avert further or consequential damage.
            </li>
            <li className="text-sm text-gray-600">
            Report the incident to our customer service hotline at (02) 8845-1111 and press 1 for towing assistance.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Insured's Authorized Representative</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            The Insured may authorize his representative through a special of attorney or a secretary's certificate (if a corporation)delegating the specific scope of authorities to officially transact in his behalf.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Fair Market Value at the Time of Loss</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            In the event that the vehicle was extensively damaged and assessed to be a constructive total loss, please note that your Insurer’s liability shall be based on the fair market or actual cash value of the vehicle at the time of loss in accordance with Condition No. 2 of Section III-Loss or Damage.
            </li>
          </ul>
        </li>
        <li className="flow-root mt-4">
          <p
            className="flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
          >
            <span className="ml-1 font-bold">Third Party Liability</span>
          </p>
          <ul  className="list-disc ml-7 space-y-2 mt-2">
            <li className="text-sm text-gray-600">
            On third party property damage (TPPD) claim, the Insured does not pay for any deductible or depreciation, but shall assume the excess amount of damage beyond the Policy’s limit of liability or sum insured for TPPD cover.
            </li>
            <li className="text-sm text-gray-600">
            For the bodily injury claim (death and/or injuries to the victim/s), 
            the computation of your Insurer’s Liability shall be based on the Schedule of Indemnities as stated in the Policy. Please refer to Section I Liability to the Public. 
            Established Medical Expenses not covered under CTPL may be claimed under an Excess Liability Insurance, if this cover was purchased.
            </li>
            <li className="text-sm text-gray-600">
            Do not make any settlement without the approval of your Insurer especially when the driver of the insured unit is not at fault.
            </li>
            <li className="text-sm text-gray-600">
            If there is an immediate need to release financial assistance for death or injury claims, 
            ake sure that that all expenses are receipted and acknowledged by the third-party victim or the established legal heirs.
            </li>
            <li className="text-sm text-gray-600">
            We highly suggest that any emergency financial assistance provided to a third party must be 
            duly documented and executed at the police station or in the presence of the police or traffic officer handling the incident. 
            A copy of a valid government issued ID of the third party should also be obtained.
            </li>
            <li className="text-sm text-gray-600">
            Should any legal action be filed in court, secure documentation pertaining to the case and promptly submit to Standard Insurance.
            </li>
          </ul>
        </li>
      </ul>
    </>
  )
}

export default MotorcarClaimServicingPopoverList;