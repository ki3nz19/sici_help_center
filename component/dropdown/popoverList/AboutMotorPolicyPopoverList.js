import { popovers } from "../../../data/dropdown"

const AboutMotorPolicyPopoverList = () => {
  return (
    <>
      <ul role="list" className="mt-3">
        <li className="ml-1 flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150">
          A. What are the coverages of my motor policy?
        </li>
        {popovers.aboutMotorPolicy.map((item, index) => (
          <li key={index} className="flow-root space-y-1">
            <p
              className="-ml-3 flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150"
            >
              <span className="ml-4">{item.name}</span>
            </p>
            <ul className="list-disc pl-4">
              <li className="font-normal text-sm ml-1 text-gray-600 list-disc mt-1">
                {item.description}
                <p className="font-normal text-sm text-gray-600 mt-1">{item.sub}</p>
              </li>
            </ul>
          </li>
        ))}
        <li className="font-normal text-sm text-gray-600 ml-5 mt-1">
          <p>a. Death </p>
          <p>b. Permanent Disablement or Dismemberment</p>
          <p>c. Medical or Surgical Treatment</p>
        </li>
        <li className="ml-1 flex items-center rounded-md text-base font-semibold text-gray-900 hover:bg-gray-50 transition ease-in-out duration-150">
          B. What are the exclusions of my motor policy? 
        </li>
        <li className="font-normal text-sm text-gray-600 ml-1 mt-1">
          Please take note that only accident-related damages will be covered under your vehicle’s insurance coverage.  The Policy excludes the following:
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          1. Loss or damage arising from an Insurance Policy with unpaid premium.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          <p>2. Policy Deductible:</p>
          <li className="list-disc ml-5 mt-1">
            In every event or incident of an Own Damage Claim, you would have to pay/shoulder a Deductible Amount declared in your Policy. 
            Please refer to Condition No. 1 of Exceptions to Section III of your Policy.
           Multiple damages may be claimed once but the number of deductibles applied shall be based on the established number of incidents/or events the insured vehicle was involved in.
          </li>
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          <p>3. Betterment/Depreciation</p>
          <li className="list-disc ml-5 mt-1">
            The client has to share in the cost of brand new parts for replacement for insured vehicle above three (3) years of age. 
            This is commonly known as Insured’s Participation or depreciation representing the reduction in value of vehicle parts or components due to normal wear and tear. 
            Kindly check Condition No. 3 of Section III-Loss or Damage for the Schedule of Depreciation.
          </li>
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          4. Damage or loss attributable to animals or pests is not covered by the Policy.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          5. Deteriorated, worn or broken components (wear and tear) mechanical and electrical breakdowns, failures or breakages and consequential loss.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          6. Burst/blown tire due to road punctures are excluded from Policy Liability.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          7. Damage to tires (such as burst/blown tire due to road punctures), unless the Schedule Vehicle is damaged at the same time. 
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          8. Any malicious damage caused by the Insured, any member of his family or by a person in the Insured’s service.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          <p>9. Any Accident, or liability caused, or incurred:</p>
          <li className="list-disc ml-5 mt-1">
            Outside the Republic of the Philippines
          </li>
          <li className="list-disc ml-5 mt-1">
            When the Insured Vehicle is used otherwise than in accordance with the Limitation As To Use Provision 
          </li>
          <li className="list-disc ml-5 mt-1">
            Being driven by any person other than an Authorized Driver
          </li>
          <li className="list-disc ml-5 mt-1">
            While the Insured Vehicle is on board a sea vessel on inter-island transit
          </li>
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          10. Usage of the Insured vehicle in unlawful or illegal acts or activities.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          11. Fraud or any attempted fraud perpetrated by the Insured or in collusion with other parties.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          12. Any liability assumed or agreed to by the Insured but which would not have attached in the absence of such agreement, 
          except liability arising out of an on spot agreement or amicable settlement of minor accident to avoid impairing the flow of traffic.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          <p>13. Loss or damage or liability proximate directly or indirectly, proximately or remotely occasioned by, contributed by or traceable to, or arising out of, or in connection with:</p>
          <li className="list-disc ml-5 mt-1">
          invasion, the act of foreign enemies, hostilities or warlike operations (whether war be declared or not), 
          mutiny, rebellion, insurrection, military or usurped power, or by any direct or indirect consequences of any of the said occurrences
          </li>
          <li className="list-disc ml-5 mt-1">
          flood, typhoon, hurricane, volcanic eruption, earthquake and other convulsions of nature, unless this Exception is bought back as AON Cover.
          </li>
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          14. Any sum which the Insured would have been entitled to recover from any part but for an agreement between the Insured and such party.
        </li>
        <li className="font-normal text-sm text-gray-600 ml-2 mt-1">
          15. Bodily injury and/or death to pay person in the employ of the Insured arising out of and in the course of such employment, of bodily injury and/or death to any member of the Insured’s household who is riding in the Schedule Vehicle.
        </li>
      </ul>
    </>
  )
}

export default AboutMotorPolicyPopoverList;