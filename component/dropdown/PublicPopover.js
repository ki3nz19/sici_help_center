/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/solid'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

const PublicPopover = ({title, children}) => {
  return (
    <>
      <Popover className="z-0 relative">
        {({ open }) => (
        <>
          <div className="relative z-10">
            <div className="max-w-7xl mx-auto flex">
              <Popover.Button
                className={classNames(
                  open ? 'text-gray-100 dropdown-bg' : 'text-gray-500 bg-white',
                  'group rounded-md border-gray-200 border-2 py-2 px-4 inline-flex justify-between w-full items-center text-base font-medium'
                )}
              >
                <span>{title}</span>
                <ChevronDownIcon
                  className={classNames(
                    open ? 'text-gray-100' : 'text-gray-400',
                    'ml-2 h-5 w-5'
                  )}
                  aria-hidden="true"
                />
              </Popover.Button>
            </div>
          </div>

          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 -translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 -translate-y-1"
          >
            <Popover.Panel className="relative z-10 inset-x-0 transform rounded-lg border-2 bg-white border-gray-100 shadow-xl">
              <div className="absolute inset-0 flex" aria-hidden="true">
                <div className="bg-white w-full rounded-md" />
              </div>
              <div className="relative max-w-7xl mx-auto grid">
                <nav
                  className="grid bg-white py-8 sm:px-6 px-8 xl:pr-12"
                  aria-labelledby="solutions-heading"
                >
                  <div>
                    <h3 className="text-base font-bold tracking-wide text-gray-700">{title}</h3>
                    {/* Drop Popover List here */}
                    {children}
                    </div>
                    </nav>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    </>
  );
}

export default PublicPopover;